package com.arisgeorgoulis.moviesapp.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by arisg on 2/21/2018.
 */

public class Movie implements Parcelable {
    String ID;
    String title;
    String releaseDate;
    String poster_img;
    String voteAVG;
    String plotSynopsis;

    public Movie(String id, String title, String releaseDate, String poster_img, String voteAVG, String plotSynopsis) {
        this.ID = id;
        this.title = title;
        this.releaseDate = releaseDate;
        this.poster_img = poster_img;
        this.voteAVG = voteAVG;
        this.plotSynopsis = plotSynopsis;
    }

    protected Movie(Parcel in) {
        ID = in.readString();
        title = in.readString();
        releaseDate = in.readString();
        poster_img = in.readString();
        voteAVG = in.readString();
        plotSynopsis = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ID);
        dest.writeString(title);
        dest.writeString(releaseDate);
        dest.writeString(poster_img);
        dest.writeString(voteAVG);
        dest.writeString(plotSynopsis);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getPoster_img() {
        return poster_img;
    }

    public void setPoster_img(String poster_img) {
        this.poster_img = poster_img;
    }

    public String getVoteAVG() {
        return voteAVG;
    }

    public void setVoteAVG(String voteAVG) {
        this.voteAVG = voteAVG;
    }

    public String getPlotSynopsis() {
        return plotSynopsis;
    }

    public void setPlotSynopsis(String plotSynopsis) {
        this.plotSynopsis = plotSynopsis;
    }

}
