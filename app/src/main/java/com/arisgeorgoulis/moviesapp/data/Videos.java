package com.arisgeorgoulis.moviesapp.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by arisg on 3/26/2018.
 */

public class Videos implements Parcelable{
    private String key;
    private String name;
    private String site;


    public Videos(String key, String name, String site) {
        this.key = key;
        this.name = name;
        this.site = site;
    }

    protected Videos(Parcel in) {
        key = in.readString();
        name = in.readString();
        site = in.readString();
    }

    public static final Creator<Videos> CREATOR = new Creator<Videos>() {
        @Override
        public Videos createFromParcel(Parcel in) {
            return new Videos(in);
        }

        @Override
        public Videos[] newArray(int size) {
            return new Videos[size];
        }
    };

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(key);
        dest.writeString(name);
        dest.writeString(site);
    }
}
