package com.arisgeorgoulis.moviesapp.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by arisg on 3/25/2018.
 */

public final class Review implements Parcelable{
    private String username;
    private String reviewByUser;


    public Review(String username, String reviewByUser) {
        this.username = username;
        this.reviewByUser = reviewByUser;
    }

    protected Review(Parcel in) {
        username = in.readString();
        reviewByUser = in.readString();
    }

    public static final Creator<Review> CREATOR = new Creator<Review>() {
        @Override
        public Review createFromParcel(Parcel in) {
            return new Review(in);
        }

        @Override
        public Review[] newArray(int size) {
            return new Review[size];
        }
    };

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getReviewByUser() {
        return reviewByUser;
    }

    public void setReviewByUser(String reviewByUser) {
        this.reviewByUser = reviewByUser;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(reviewByUser);
    }
}
