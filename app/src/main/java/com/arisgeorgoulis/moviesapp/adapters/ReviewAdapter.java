package com.arisgeorgoulis.moviesapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arisgeorgoulis.moviesapp.R;
import com.arisgeorgoulis.moviesapp.data.Review;

import java.util.ArrayList;

/**
 * Created by arisg on 3/25/2018.
 */

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ReviewAdapterViewHolder> {
    private ArrayList<Review> reviews;

    public ReviewAdapter(ArrayList<Review> reviews) {
        this.reviews = reviews;
    }

    @NonNull
    @Override
    public ReviewAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForRecyclerItem = R.layout.movie_review_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;
        View view = inflater.inflate(layoutIdForRecyclerItem, parent, shouldAttachToParentImmediately);
        return new ReviewAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewAdapterViewHolder holder, int position) {
        Review review = this.reviews.get(position);
        holder.userNameHere.setText(review.getUsername());
        holder.descHere.setText(review.getReviewByUser());
    }

    public void reviewsHaveUpdated(ArrayList<Review> reviews) {
        this.reviews = reviews;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (reviews == null) {
            return 0;
        } else {
            return reviews.size();
        }
    }

    public class ReviewAdapterViewHolder extends RecyclerView.ViewHolder {
        private final TextView userNameHere, descHere;

        public ReviewAdapterViewHolder(View itemView) {
            super(itemView);

            userNameHere = (TextView) itemView.findViewById(R.id.reviewer_name_here);
            descHere = (TextView) itemView.findViewById(R.id.reviewer_desc_here);
        }
    }
}
