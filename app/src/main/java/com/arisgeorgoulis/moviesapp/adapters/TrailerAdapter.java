package com.arisgeorgoulis.moviesapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arisgeorgoulis.moviesapp.R;
import com.arisgeorgoulis.moviesapp.data.Videos;
import java.util.ArrayList;

/**
 * Created by arisg on 3/27/2018.
 */

public class TrailerAdapter extends RecyclerView.Adapter<TrailerAdapter.TrailerAdapterViewHolder>{
    private ArrayList<Videos> videos;
    private final VideoAdapterOnClickHandler videoAdapterOnClickHandler;

    public interface VideoAdapterOnClickHandler {
        void onClick(Videos video);
    }

    public TrailerAdapter(VideoAdapterOnClickHandler videoAdapterOnClickHandler, ArrayList<Videos> videos) {
        this.videoAdapterOnClickHandler = videoAdapterOnClickHandler;
        this.videos = videos;
    }

    @NonNull
    @Override
    public TrailerAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForRecyclerItem = R.layout.movie_trailer_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;
        View view = inflater.inflate(layoutIdForRecyclerItem, parent, shouldAttachToParentImmediately);
        return new TrailerAdapterViewHolder(view);    }

    @Override
    public void onBindViewHolder(@NonNull TrailerAdapterViewHolder holder, int position) {
        Videos video = this.videos.get(position);
        holder.trailer_name.setText(video.getName());
    }

    public void videosHaveUpdated(ArrayList<Videos> videos) {
        this.videos = videos;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (videos == null) {
            return 0;
        } else {
            return videos.size();
        }
    }

    public class TrailerAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private final TextView trailer_name;

        public TrailerAdapterViewHolder(View itemView) {
            super(itemView);

            trailer_name = (TextView) itemView.findViewById(R.id.trailer_desc);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            Videos video = videos.get(clickedPosition);
            videoAdapterOnClickHandler.onClick(video);
        }
    }
}
