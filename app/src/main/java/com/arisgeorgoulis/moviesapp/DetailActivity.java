package com.arisgeorgoulis.moviesapp;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arisgeorgoulis.moviesapp.adapters.ReviewAdapter;
import com.arisgeorgoulis.moviesapp.adapters.TrailerAdapter;
import com.arisgeorgoulis.moviesapp.contentprovider.MovieContract;
import com.arisgeorgoulis.moviesapp.data.Movie;
import com.arisgeorgoulis.moviesapp.data.Review;
import com.arisgeorgoulis.moviesapp.data.Videos;
import com.arisgeorgoulis.moviesapp.utilities.NetworkUtils;
import com.arisgeorgoulis.moviesapp.utilities.ReviewAPIClient;
import com.arisgeorgoulis.moviesapp.utilities.VideoAPIClient;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity implements TrailerAdapter.VideoAdapterOnClickHandler,ReviewAPIClient.UpdateTheReviewAdapterListener<ArrayList<Review>>, VideoAPIClient.UpdateTheVideoAdapterListener<ArrayList<Videos>> {
    private TextView movieTitle;
    private TextView movieRelease;
    private TextView movieAVG;
    private TextView movieOverview;
    private ImageView moviePoster;
    private ArrayList<Review> reviews;
    private ArrayList<Videos> videos;
    private RecyclerView reviewRecycler;
    private ReviewAdapter reviewAdapter;
    private LinearLayoutManager reviewLayoutManager;
    private ReviewAPIClient reviewAPIClient;
    private RecyclerView trailerRecycler;
    private TrailerAdapter trailerAdapter;
    private LinearLayoutManager trailerLayoutManager;
    private VideoAPIClient videoAPIClient;
    private ImageButton favButton;
    private Movie movie;
    private boolean IS_FAVOURITE;

    private final static String DETAIL_STATE = "detailState";
    private final static String DETAIL_STATE_REV = "detailStateRev";
    private final static String DETAIL_STATE_VID = "detailStateVid";
    private final static String REVIEW_POS = "reviewPos";
    private final static String VIDEO_POS = "videoPos";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        favButton = (ImageButton) this.findViewById(R.id.favButton);
        reviewRecycler = (RecyclerView) this.findViewById(R.id.movie_reviews_recycler_view);
        trailerRecycler = (RecyclerView) this.findViewById(R.id.movie_trailer_recycler_view);
        reviewLayoutManager = new LinearLayoutManager(DetailActivity.this, LinearLayoutManager.HORIZONTAL, false);
        trailerLayoutManager = new LinearLayoutManager(DetailActivity.this, LinearLayoutManager.VERTICAL, false);
        reviewRecycler.setLayoutManager(reviewLayoutManager);
        reviewRecycler.setHasFixedSize(false);
        reviewAdapter = new ReviewAdapter(this.reviews);
        reviewRecycler.setAdapter(reviewAdapter);
        reviewAPIClient = new ReviewAPIClient(DetailActivity.this);
        trailerRecycler.setLayoutManager(trailerLayoutManager);
        trailerRecycler.setHasFixedSize(false);
        trailerAdapter = new TrailerAdapter(this, this.videos);
        trailerRecycler.setAdapter(trailerAdapter);
        videoAPIClient = new VideoAPIClient(DetailActivity.this);
        movieTitle = (TextView) this.findViewById(R.id.movie_title_detail);
        moviePoster = (ImageView) this.findViewById(R.id.movie_poster_detail);
        movieRelease = (TextView) this.findViewById(R.id.movie_release_detail);
        movieAVG = (TextView) this.findViewById(R.id.movie_avg_detail);
        movieOverview = (TextView) this.findViewById(R.id.movie_overview_detail);
        Intent intent = getIntent();
        if (savedInstanceState == null) {
            movie = intent.getParcelableExtra(MainActivity.MOVIE_PARC);
        } else {
            Log.d("savedInstance founded", "!");
            movie = savedInstanceState.getParcelable(DETAIL_STATE);
        }
        movieTitle.setText(movie.getTitle());
        isFav(movie);
        if (savedInstanceState == null) {
            if (NetworkUtils.isOnline(DetailActivity.this)) {
                Picasso.with(this.moviePoster.getContext()).load("http://image.tmdb.org/t/p/w185/" + movie.getPoster_img()).into(this.moviePoster);
                reviewAPIClient.getTheMovieReviews(movie.getID());
                videoAPIClient.getTheMovieVideos(movie.getID());
            } else {
                Toast.makeText(DetailActivity.this, "No internet connection found !", Toast.LENGTH_LONG).show();
            }
        } else {
            if (NetworkUtils.isOnline(DetailActivity.this)) {
                Picasso.with(this.moviePoster.getContext()).load("http://image.tmdb.org/t/p/w185/" + movie.getPoster_img()).into(this.moviePoster);
            }
            this.reviews = savedInstanceState.getParcelableArrayList(DETAIL_STATE_REV);
            this.videos = savedInstanceState.getParcelableArrayList(DETAIL_STATE_VID);
            reviewAdapter.reviewsHaveUpdated(this.reviews);
            trailerAdapter.videosHaveUpdated(this.videos);
            if (savedInstanceState.getInt(REVIEW_POS) != -1) {
                reviewRecycler.smoothScrollToPosition(savedInstanceState.getInt(REVIEW_POS));
            }
            if (savedInstanceState.getInt(VIDEO_POS) != -1) {
                trailerRecycler.smoothScrollToPosition(savedInstanceState.getInt(VIDEO_POS));
            }
        }
        movieRelease.setText(movie.getReleaseDate());
        movieAVG.setText(movie.getVoteAVG());
        movieOverview.setText(movie.getPlotSynopsis());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(DETAIL_STATE, this.movie);
        outState.putParcelableArrayList(DETAIL_STATE_REV, this.reviews);
        outState.putParcelableArrayList(DETAIL_STATE_VID, this.videos);
        outState.putInt(REVIEW_POS, reviewLayoutManager.findLastVisibleItemPosition());
        outState.putInt(VIDEO_POS, trailerLayoutManager.findLastVisibleItemPosition());
    }

    @Override
    public void updateTheAdapter(ArrayList<Review> reviews) {
        this.reviews = reviews;
        reviewAdapter.reviewsHaveUpdated(reviews);
    }

    @Override
    public void updateTheVideoAdapter(ArrayList<Videos> videos) {
        this.videos = videos;
        trailerAdapter.videosHaveUpdated(videos);
    }

    public void favOrUnfavMovie (View view) {
        if (IS_FAVOURITE == true) {
            String id = movie.getID();
            int row = getContentResolver().delete(ContentUris.withAppendedId(MovieContract.MovieEntry.CONTENT_URI, Long.parseLong(id)), null, null);
            favButton.setImageResource(android.R.drawable.btn_star_big_off);
            this.IS_FAVOURITE = false;
        } else {
            ContentValues contentValues = new ContentValues();

            String id, title, release, poster, avg, synopsis;

            id = movie.getID();
            title = movie.getTitle();
            release = movie.getReleaseDate();
            poster = movie.getPoster_img();
            avg = movie.getVoteAVG();
            synopsis = movie.getPlotSynopsis();


            contentValues.put(MovieContract.MovieEntry._ID, id);
            contentValues.put(MovieContract.MovieEntry.COLUMN_TITLE, title);
            contentValues.put(MovieContract.MovieEntry.COLUMN_RELEASE_DATE, release);
            contentValues.put(MovieContract.MovieEntry.COLUMN_MOVIE_IMG, poster);
            contentValues.put(MovieContract.MovieEntry.COLUMN_AVG_VOTE, avg);
            contentValues.put(MovieContract.MovieEntry.COLUMN_DESCRIPTION, synopsis);

            Uri mUri = getContentResolver().insert(MovieContract.MovieEntry.CONTENT_URI, contentValues);

            favButton.setImageResource(android.R.drawable.btn_star_big_on);
            this.IS_FAVOURITE = true;
        }
    }

    public void isFav (Movie movie) {
        String id = movie.getID();

        String[] projection = {MovieContract.MovieEntry._ID};
        // Defines a string to contain the selection clause
        String mSelectionClause = null;

        // Initializes an array to contain selection arguments
        String[] mSelectionArgs = {""};

        mSelectionClause = MovieContract.MovieEntry._ID + " = ?";

        // Moves the user's input string to the selection arguments.
        mSelectionArgs[0] = id;
        Cursor cursor = getContentResolver().query(MovieContract.MovieEntry.CONTENT_URI, projection, mSelectionClause, mSelectionArgs, null);

        Log.d("The cursor returned :", String.valueOf(cursor.getCount()));

        if (cursor.getCount() == 0) {
            favButton.setImageResource(android.R.drawable.btn_star_big_off);
            this.IS_FAVOURITE = false;
        } else {
            favButton.setImageResource(android.R.drawable.btn_star_big_on);
            this.IS_FAVOURITE = true;
        }
    }

    @Override
    public void onClick(Videos video) {
        switch (video.getSite()) {
            case "YouTube":
                String key = video.getKey();
                String source = "https://www.youtube.com/watch?v=" + key;
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(source)));
                break;
            default:
                Toast.makeText(DetailActivity.this, "We cannot recognize the site", Toast.LENGTH_LONG).show();
        }
    }
}
