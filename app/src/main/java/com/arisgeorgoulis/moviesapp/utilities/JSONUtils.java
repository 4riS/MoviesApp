package com.arisgeorgoulis.moviesapp.utilities;

import android.util.Log;

import com.arisgeorgoulis.moviesapp.data.Movie;
import com.arisgeorgoulis.moviesapp.data.Review;
import com.arisgeorgoulis.moviesapp.data.Videos;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arisg on 2/23/2018.
 */

public final class JSONUtils {
    private final static String NODE_RESULTS = "results";
    private final static String MOVIE_ID = "id";
    private final static String MOVIE_TITLE = "title";
    private final static String MOVIE_POSTER = "poster_path";
    private final static String MOVIE_RELEASE_DATE = "release_date";
    private final static String MOVIE_VOTE_AVG = "vote_average";
    private final static String MOVIE_PLOT_SYNOPSIS = "overview";
    private final static String REVIEW_USERNAME = "author";
    private final static String REVIEW_DESC = "content";
    private final static String VIDEO_KEY = "key";
    private final static String VIDEO_NAME = "name";
    private final static String VIDEO_SITE = "site";

    public static ArrayList<Movie> parseMovieJson(String json) {
        ArrayList<Movie> movies = new ArrayList<>();
        try {
            JSONObject mainRoot = new JSONObject(json);
            JSONArray results = mainRoot.optJSONArray(NODE_RESULTS);

            for (int i = 0; i < results.length(); i++) {
                JSONObject eachMovie = results.optJSONObject(i);
                String id = eachMovie.optString(MOVIE_ID);
                String title = eachMovie.optString(MOVIE_TITLE);
                String release_date = eachMovie.optString(MOVIE_RELEASE_DATE);
                String poster_img = eachMovie.optString(MOVIE_POSTER);
                String voteAVG = eachMovie.optString(MOVIE_VOTE_AVG);
                String plotSynopsis = eachMovie.optString(MOVIE_PLOT_SYNOPSIS);
                movies.add(new Movie(id, title, release_date, poster_img, voteAVG, plotSynopsis));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return movies;
    }

    public static ArrayList<Review> parseReviewJson(String json) {
        ArrayList<Review> reviews = new ArrayList<>();
        try {
            JSONObject mainRoot = new JSONObject(json);
            JSONArray results = mainRoot.optJSONArray(NODE_RESULTS);

            Log.d("The data are", mainRoot.toString());

            for (int i = 0; i < results.length(); i++) {
                JSONObject eachMovie = results.optJSONObject(i);
                String username = eachMovie.optString(REVIEW_USERNAME);
                String desc = eachMovie.optString(REVIEW_DESC);
                reviews.add(new Review(username, desc));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return reviews;
    }

    public static ArrayList<Videos> parseVideoJson(String json) {
        ArrayList<Videos> videos = new ArrayList<>();
        try {
            JSONObject mainRoot = new JSONObject(json);
            JSONArray results = mainRoot.optJSONArray(NODE_RESULTS);

            Log.d("The data are", mainRoot.toString());

            for (int i = 0; i < results.length(); i++) {
                JSONObject eachMovie = results.optJSONObject(i);
                String key = eachMovie.optString(VIDEO_KEY);
                String video_name = eachMovie.optString(VIDEO_NAME);
                String site = eachMovie.optString(VIDEO_SITE);
                videos.add(new Videos(key, video_name, site));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return videos;
    }
}
