package com.arisgeorgoulis.moviesapp.utilities;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by arisg on 3/19/2018.
 */

public interface MovieAPI {
    static final String BASE_URL = "https://api.themoviedb.org/3/movie/";
    static final String API_KEY_PARAMETER = "api_key";
    static final String API_KEY = "cc77b819c44ddd403d44f1db25f2e1e4";

    @GET("{show_choice}")
    Call<ResponseBody> getMovies(@Path("show_choice") String showMoviePref, @QueryMap Map<String, String> query_parameters);

    @GET("{id}/reviews")
    Call<ResponseBody> getMoviesReviews(@Path("id") String movieID, @QueryMap Map<String, String> query_parameters);

    @GET("{id}/videos")
    Call<ResponseBody> getMoviesVideos(@Path("id") String movieID, @QueryMap Map<String, String> query_parameters);
}