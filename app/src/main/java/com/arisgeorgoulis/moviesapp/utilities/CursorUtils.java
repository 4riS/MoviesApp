package com.arisgeorgoulis.moviesapp.utilities;

import android.database.Cursor;

import com.arisgeorgoulis.moviesapp.contentprovider.MovieContract;
import com.arisgeorgoulis.moviesapp.data.Movie;

import java.util.ArrayList;

public class CursorUtils {
    public final static ArrayList<Movie> cursorToMovies(Cursor cursor){
        ArrayList<Movie> movies = new ArrayList<>();

        if (cursor.moveToFirst()){
            do{
                String id = cursor.getString(cursor.getColumnIndex(MovieContract.MovieEntry._ID));
                String title = cursor.getString(cursor.getColumnIndex(MovieContract.MovieEntry.COLUMN_TITLE));
                String releaseDate = cursor.getString(cursor.getColumnIndex(MovieContract.MovieEntry.COLUMN_RELEASE_DATE));
                String movieIMG = cursor.getString(cursor.getColumnIndex(MovieContract.MovieEntry.COLUMN_MOVIE_IMG));
                String avg = cursor.getString(cursor.getColumnIndex(MovieContract.MovieEntry.COLUMN_AVG_VOTE));
                String synopsis = cursor.getString(cursor.getColumnIndex(MovieContract.MovieEntry.COLUMN_DESCRIPTION));

                // Add the movie to arraylist
                movies.add(new Movie(id, title, releaseDate, movieIMG, avg, synopsis));
            }while(cursor.moveToNext());
        }
        cursor.close();

        return movies;
    }
}
