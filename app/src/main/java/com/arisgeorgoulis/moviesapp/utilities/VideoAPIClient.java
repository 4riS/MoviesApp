package com.arisgeorgoulis.moviesapp.utilities;

import android.util.Log;

import com.arisgeorgoulis.moviesapp.data.Videos;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by arisg on 3/26/2018.
 */

public class VideoAPIClient implements Callback<ResponseBody> {
    private final UpdateTheVideoAdapterListener<ArrayList<Videos>> listener;

    public VideoAPIClient(UpdateTheVideoAdapterListener<ArrayList<Videos>> listener) {
        this.listener = listener;
    }

    public void getTheMovieVideos(String id) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MovieAPI.BASE_URL)
                .build();

        Map<String, String> data = new HashMap<>();
        data.put(MovieAPI.API_KEY_PARAMETER, MovieAPI.API_KEY);


        MovieAPI service = retrofit.create(MovieAPI.class);

        Call<ResponseBody> call = service.getMoviesVideos(id, data);

        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        String jsonDataFromAPI = null;
        try {
            jsonDataFromAPI = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayList<Videos> videos = JSONUtils.parseVideoJson(jsonDataFromAPI);
        listener.updateTheVideoAdapter(videos);
    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        Log.e("Response has failed :", t.toString());
    }

    public interface UpdateTheVideoAdapterListener<T> {
        void updateTheVideoAdapter(T t);
    }
}
