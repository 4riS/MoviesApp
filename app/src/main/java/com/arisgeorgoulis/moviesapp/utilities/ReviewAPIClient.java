package com.arisgeorgoulis.moviesapp.utilities;

import android.util.Log;

import com.arisgeorgoulis.moviesapp.data.Review;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by arisg on 3/25/2018.
 */

public class ReviewAPIClient implements Callback<ResponseBody> {
    private final UpdateTheReviewAdapterListener<ArrayList<Review>> listener;

    public ReviewAPIClient(UpdateTheReviewAdapterListener<ArrayList<Review>> listener) {
        this.listener = listener;
    }

    public void getTheMovieReviews(String id) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MovieAPI.BASE_URL)
                .build();

        Map<String, String> data = new HashMap<>();
        data.put(MovieAPI.API_KEY_PARAMETER, MovieAPI.API_KEY);


        MovieAPI service = retrofit.create(MovieAPI.class);

        Call<ResponseBody> call = service.getMoviesReviews(id, data);

        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        String jsonDataFromAPI = null;
        try {
            jsonDataFromAPI = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayList<Review> reviews = JSONUtils.parseReviewJson(jsonDataFromAPI);
        listener.updateTheAdapter(reviews);
    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        Log.e("Response has failed :", t.toString());
    }

    public interface UpdateTheReviewAdapterListener<T> {
        void updateTheAdapter(T t);
    }
}
