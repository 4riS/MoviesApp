package com.arisgeorgoulis.moviesapp.contentprovider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MovieDBHelper extends SQLiteOpenHelper {
    // The name of the database
    private static final String DATABASE_NAME = "movieDb.db";

    // If you change the database schema, you must increment the database version
    private static final int VERSION = 1;

    public MovieDBHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_FAVORITES_TABLE = "CREATE TABLE " + MovieContract.MovieEntry.TABLE_NAME + "(" +
                MovieContract.MovieEntry._ID + " TEXT NOT NULL PRIMARY KEY,  "+
                MovieContract.MovieEntry.COLUMN_TITLE + " TEXT NOT NULL ,"+
                MovieContract.MovieEntry.COLUMN_RELEASE_DATE + " TEXT ,"+
                MovieContract.MovieEntry.COLUMN_MOVIE_IMG + " TEXT ,"+
                MovieContract.MovieEntry.COLUMN_AVG_VOTE + " TEXT ,"+
                MovieContract.MovieEntry.COLUMN_DESCRIPTION + " TEXT "+");";
        db.execSQL(SQL_CREATE_FAVORITES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+  MovieContract.MovieEntry.TABLE_NAME);
        onCreate(db);
    }
}
