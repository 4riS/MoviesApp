package com.arisgeorgoulis.moviesapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.arisgeorgoulis.moviesapp.adapters.MovieAdapter;
import com.arisgeorgoulis.moviesapp.contentprovider.MovieContract;
import com.arisgeorgoulis.moviesapp.data.Movie;
import com.arisgeorgoulis.moviesapp.utilities.CursorUtils;
import com.arisgeorgoulis.moviesapp.utilities.JSONUtils;
import com.arisgeorgoulis.moviesapp.utilities.MovieAPIClient;
import com.arisgeorgoulis.moviesapp.utilities.NetworkUtils;

import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MovieAdapter.MovieAdapterOnClickHandler, SharedPreferences.OnSharedPreferenceChangeListener, MovieAPIClient.updateTheAdapterListener<ArrayList<Movie>> {
    private RecyclerView recyclerView;
    private MovieAdapter movieAdapter;
    private GridLayoutManager gridLayoutManager;
    private ArrayList<Movie> movies;
    private MovieAPIClient movieAPIClient;
    private String whatChoiceIsVisible;
    private int mCurrentPosition;

    private static final String STATE = "state";
    private static final String STATE_LIST = "stateList";
    public final static String MOVIE_PARC = "movie_parcelable";
    private final int NUM_OF_COL = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        movies = null;
        recyclerView = (RecyclerView) this.findViewById(R.id.movie_recycler_view);
        gridLayoutManager = new GridLayoutManager(this, NUM_OF_COL);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(false);
        movieAdapter = new MovieAdapter(MainActivity.this, movies);
        recyclerView.setAdapter(movieAdapter);
        movieAPIClient = new MovieAPIClient(MainActivity.this);
        whatChoiceIsVisible = getChoiceOfShowingMovie();
        if (savedInstanceState != null) {
            this.movies = savedInstanceState.getParcelableArrayList(STATE_LIST);
            mCurrentPosition = savedInstanceState.getInt(STATE);
            movieAdapter.moviesHaveUpdate(this.movies);
            if (mCurrentPosition != -1) {
                recyclerView.smoothScrollToPosition(mCurrentPosition);
            }
        }
        else {
            if (whatChoiceIsVisible.equals(getString(R.string.favourites_value))) {
                String[] projection = {MovieContract.MovieEntry._ID,
                        MovieContract.MovieEntry.COLUMN_TITLE,
                        MovieContract.MovieEntry.COLUMN_RELEASE_DATE,
                        MovieContract.MovieEntry.COLUMN_MOVIE_IMG,
                        MovieContract.MovieEntry.COLUMN_AVG_VOTE,
                        MovieContract.MovieEntry.COLUMN_DESCRIPTION};
                Cursor mCur = getContentResolver().query(MovieContract.MovieEntry.CONTENT_URI, projection, null, null, null);
                ArrayList<Movie> movies = CursorUtils.cursorToMovies(mCur);
                this.movies = movies;
                movieAdapter.moviesHaveUpdate(movies);
            } else {
                if (NetworkUtils.isOnline(MainActivity.this)) {
                    Log.d("The choice is :", whatChoiceIsVisible);
                    movieAPIClient.getTheMovies(whatChoiceIsVisible);
                } else {
                    Toast.makeText(MainActivity.this, "No internet connection found !", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        int mCurrentPosition = gridLayoutManager.findLastVisibleItemPosition();

        Log.d("The position saved is ", String.valueOf(mCurrentPosition));
        Log.d("The movies have size", String.valueOf(this.movies.size()));
        savedInstanceState.putInt(STATE, mCurrentPosition);
        savedInstanceState.putParcelableArrayList(STATE_LIST, this.movies);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
    }

    private String getChoiceOfShowingMovie() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String choice = sharedPreferences.getString(getString(R.string.pref_show_key), getResources().getString(R.string.default_pref_setting_popular));
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        return choice;
    }

    @Override
    public void onClick(Movie movie) {
        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        intent.putExtra(MOVIE_PARC, movie);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.settings_menu_button) {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void updateTheAdapter(ArrayList<Movie> movies) {
        this.movies = movies;
        movieAdapter.moviesHaveUpdate(movies);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.pref_show_key))) {
            if (getString(R.string.favourites_value).equals(sharedPreferences.getString(key, "popular"))) {
                String[] projection = {MovieContract.MovieEntry._ID,
                    MovieContract.MovieEntry.COLUMN_TITLE,
                    MovieContract.MovieEntry.COLUMN_RELEASE_DATE,
                    MovieContract.MovieEntry.COLUMN_MOVIE_IMG,
                    MovieContract.MovieEntry.COLUMN_AVG_VOTE,
                    MovieContract.MovieEntry.COLUMN_DESCRIPTION};
                Cursor mCur = getContentResolver().query(MovieContract.MovieEntry.CONTENT_URI, projection, null, null, null);
                ArrayList<Movie> movies = CursorUtils.cursorToMovies(mCur);
                this.movies = movies;
                movieAdapter.moviesHaveUpdate(movies);
                Log.d("Favourites selexted", "!");
            }
            else {
                String prefIs = sharedPreferences.getString(key, getString(R.string.default_pref_setting_popular));
                movieAPIClient = new MovieAPIClient(this);
                movieAPIClient.getTheMovies(prefIs);
            }
        }
    }
}
